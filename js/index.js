$(function() {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $(".carousel").carousel({
        interval: 2500
    });

    $("#contactoModal").on('show.bs.modal', function (e) {
        $("[data-target='#contactoModal']").removeClass('btn-outline-success');
        $("[data-target='#contactoModal']").addClass('btn-success');
        $("[data-target='#contactoModal']").prop('disabled', true);
        console.log('Evento: <show>');
    });
    $("#contactoModal").on('shown.bs.modal', function (e) {
        console.log('Evento: <shown>');
    });
    
    $("#contactoModal").on('hide.bs.modal', function (e) {
        console.log('Evento: <hide>');
    });
    $("#contactoModal").on('hidden.bs.modal', function (e) {
        $("[data-target='#contactoModal']").addClass('btn-outline-success');
        $("[data-target='#contactoModal']").removeClass('btn-success');
        $("[data-target='#contactoModal']").prop('disabled', false);
        console.log('Evento: <hidden>');
    });
});