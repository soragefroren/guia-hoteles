const { src, dest, task, watch, start, series, parallel } = require('gulp');

const sass = require('gulp-sass');
const browserSync = require('browser-sync');
const cleanCss = require('gulp-clean-css');
const imagemin = require('gulp-imagemin');
const flatmap = require('gulp-flatmap');
const htmlmin = require('gulp-htmlmin');
const uglify = require('gulp-uglify');
const usemin = require('gulp-usemin');
const rev = require('gulp-rev');
const del = require('del');

task('sass', function() {
    src('./css/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(dest('./css'));
});

task('sass:watch', function() {
    watch('./css/*.scss', ['sass']);
});

task('browser-sync', function() {
    var files = ['./*.html', './css/*.css', './images/*.{png, jpg, gif}', './js/*.js']
    browserSync.init(files, {
        server: {
            baseDir: './'
        }
    });
});

task('copyfonts', function () {
    return src('./node_modules/open-iconic/font/fonts/*.{ttf,woff,eof,svg,eot,otf}*')
        .pipe(dest('./dist/fonts'));
});

task('clean', function () {
    return del(['dist']);
});

task('imagemin', function () {
    return src('./images/*.{png,jpg,gif}')
        .pipe(imagemin({optimizationLevel: 3, progressive: true, interlaced: true}))
        .pipe(dest('dist/images'));
});

task('usemin', function () {
    return src('./*.html')
        .pipe(flatmap(function(stream, file) {
            return stream.pipe(usemin({
                html: [function() { return htmlmin({collapseWhiteSpace: true}); }],
                css: [rev()],
                js: [uglify(), rev()],
                inlinejs: [uglify()],
                inlinecss: [cleanCss(), 'concat']
            }));
        }))
        .pipe(dest('dist/'));
});

task('default', series('browser-sync', function () {
    start('sass:watch');
}));

task('build', series('clean', 'copyfonts', 'imagemin', 'usemin'));