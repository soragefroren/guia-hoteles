# README - Guia de Hoteles #

Este repositorio se creo para el Curso "Diseño de páginas Web con Booststrap 4"

### Contenido

Sitio web responsivo diseñado e implementado con base en el sistema de grillas de Bootstrap, y ejecutado a través del servidor "Lite-Server"

### Créditos

Autor: *Jorge Luis Jácome Domínguez*

######  Otros medios < [Linkedin](https://www.linkedin.com/in/jorge-luis-j%C3%A1come-dom%C3%ADnguez-44294a91/) - [Dibujando](https://dibujando.net/soragefroren) - [Facebook](https://www.facebook.com/SoraGefroren) - [Youtube](https://www.youtube.com/c/SoraGefroren) >
